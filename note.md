### initialize

#### parse input

- 输入文件名在main函数开头指定
- 默认输入边为双向边（marco10.col中的边居然是单向边，汗）
- 我比较习惯从0开始标号，所以读入点的标号后自动-1

随机种子需要自行设置， 默认为1，若设置为-1则会
```c++
srand(time(NULL));
```

### k-core decomposition
这部分基本上是按照fastcolor抄的，分解的结果保存在binPos里面。

不同的是没有给相同起始位置的k-core去重，但在main函数中做了这个的判定。

### local search
local search目前采用的是TABUCOL算法（Hertz and Werra 1987）

固定一个颜色数$k​$，候选解形式是一个$|V|​$的划分$s = {V_0,...,V_{k-1}}$

搜索目标是最小化目标函数$f(s)​ = \sum_{i = 0}^{k - 1} |E(V_i)|​$。

由于不可能找遍所有的邻居解，搜索策略是每一步随机选择若干数量的非禁忌邻居解，选择其中最优的步进。

搜索参数设置：
- colorCount：固定的颜色数
- maxSteps：在每一个k-core中搜索的步数
- pickCount：每一步选择邻居解的个数
- tabuTenure：tabu tenure

### 贪心
由搜索到的候选解生成完整解的时候使用了一个相当naive的贪心。。。。

### 一些关于复杂度的说明
假设点数为$n$,边数为$m$,颜色数为$c$，平均度数为$d$
- kcore分解是$O(m)$
- 图使用邻接表存储，空间复杂度为$O(m)$
- 单次寻找邻居解的复杂度为$O(d)$
- tabulist使用一个$n \times c$的矩阵存储
- 在从k-core转到k-1 core时并没有清空tabu list， 感觉要清空的话代价比较高

### 个人感受

感觉最后的贪心过于naive。。。在dsjc250.5上跑的结果相当差。。