#include <bits/stdc++.h>
#include <unistd.h>
using namespace std;

typedef vector<int> vecInt;
typedef pair<int, int> pairInt;
// graph info
const int numberDelta = -1;
const int seed = -1;
int vertexCount;
vector<vecInt> adjacencyList;
// k-core
vecInt kCore, vSorted, vDegree, binPos;
int maxDegree;
// search argv and memory
int colorCount = 15;
int tabuTenure = 1;
int now, actualColCount;
vecInt currentVertex, vColor, bestSol;
vector<vecInt> currentAdjacency;
vector<vecInt> conflict;
vector<vecInt> tabuList;
vecInt unusedTime;
vector<bool> inSearch;
set<int> uncolorSet;
bool KCORE = false;
// reactive tabu tenure
int pairs[12][3] = {{10000, 10, 5}, {10000, 15, 3}, {10000, 5, 10},
		    {5000, 15, 10}, {5000, 10, 15}, {5000, 5, 20},
		    {1000, 15, 30}, {1000, 10, 50}, {1000, 5, 100},
		    {500, 5, 100},  {500, 10, 150}, {500, 15, 200}};

int numPairs = sizeof(pairs) / sizeof(int) / 3;

int pairCycles = 0;
int frequency = pairs[0][0];
int increment = pairs[0][1];
int nextPair = pairs[0][2];
int threshold = 2;
int randomTenure = 1;
float alpha = 0.6;

void parse_input(string fileName) {
	ifstream inFile(fileName.c_str());
	if (!inFile.is_open()) {
		puts("inFile error");
		exit(1);
	}


	//get vertex count
	string line;
	istringstream is;
	string p, tmp;
	do {
		getline(inFile, line);
		is.clear();
		is.str(line);
		is >> p >> tmp >> vertexCount;
	} while(p != "p");

	cout << line << endl;

	adjacencyList.resize(vertexCount);

	int u, v;
	while (inFile >> tmp >> u >> v) {
		u += numberDelta;
		v += numberDelta;
		adjacencyList[u].push_back(v);
		adjacencyList[v].push_back(u);
	}
	inFile.close();

	// 除出重边
	for (int i = 0; i < vertexCount; i++) {
		vecInt &v = adjacencyList[i];
		sort(v.begin(), v.end());
		auto it = unique(v.begin(), v.end());
		v.resize(distance(v.begin(), it));
	}
}

void kcore_decomposition() {
	static vecInt vertexIndex(vertexCount);
	binPos.resize(vertexCount + 1);
	vDegree.resize(vertexCount);
	vSorted.resize(vertexCount);

	// sort vertexs by their degrees
	maxDegree = 0;
	for (int u = 0; u < vertexCount; u++) {
		binPos[vDegree[u] = adjacencyList[u].size()]++;
		maxDegree = max(maxDegree, vDegree[u]);
	}

	for (int s = 0, i = 0; i <= maxDegree; i++) {
		int temp = binPos[i];
		binPos[i] = s;
		s += temp;
	}

	for (int u = 0; u < vertexCount; u++) {
		int index = binPos[vDegree[u]]++;
		vertexIndex[u] = index;
		vSorted[index] = u;	
	}	

	for (int i = maxDegree; i; i--)
		binPos[i] = binPos[i - 1];
	binPos[0] = 0;

	// collapse
	for (auto u : vSorted) {
		for (auto v : adjacencyList[u]) {
			if (vDegree[v] > vDegree[u]) {
				int vIndex = vertexIndex[v];
				int wIndex = binPos[vDegree[v]];
				int w = vSorted[wIndex];
				vSorted[vIndex] = w; 
				vSorted[wIndex] = v;
				vertexIndex[v] = wIndex;
				vertexIndex[w] = vIndex;
				binPos[vDegree[v]--]++;
			}
		}
	}

//	kCore = binPos;
//	kCore.resize(maxDegree + 1);
}

void initialize(string inFile) {
	parse_input(inFile);
if (seed == -1)
		srand(time(NULL));
	else 
		srand(seed);

	vColor.resize(vertexCount, -1);
	unusedTime.resize(vertexCount, -1);
	inSearch.resize(vertexCount);
	currentAdjacency.resize(vertexCount);

	tabuList.resize(vertexCount);
	conflict.resize(vertexCount);
	for (int i = 0; i < vertexCount; i++) {
		tabuList[i].resize(colorCount, -vertexCount);
		conflict[i].resize(colorCount, 0);
	}
}

inline int color_cost(int u, int color) {
	return color == -1 ? 0 : conflict[u][color];
}

inline void color_vertex(int u, int color) {
	for (auto v : currentAdjacency[u]) {
		if (vColor[u] != -1)
			conflict[v][vColor[u]]--;
		if (color != -1)
			conflict[v][color]++;
	}

	vColor[u] = color;
	if (color == -1) {
		unusedTime[u] = now;
		uncolorSet.insert(u);
	}
	else 
		uncolorSet.erase(u);
}

inline bool is_tabu(int u, int color) {
	return now < tabuList[u][color];
}

inline int rand_uncolor() {
	int r = rand() % vertexCount;
	auto p = uncolorSet.lower_bound(r);
	if (p == uncolorSet.end())
		p = uncolorSet.begin();
	return *p;
}

pairInt random_find_move(int pickCount) {
	int minCost = currentVertex.size();
	pairInt result;

	for (int i = 0; i < pickCount; ) {
		int u = rand_uncolor(); 
		int newColor = rand() % colorCount;
		
		// 计算这个候选解对目标函数的影响
		int uCost = color_cost(u, newColor);
		if (uCost || uncolorSet.size() != bestSol.size())
			if (is_tabu(u, newColor))
				continue ;
		if (uCost < minCost) {
			uCost = minCost;
			result = make_pair(u, newColor);
		}
		i++;
	}

	return result;
}

bool timeComp(int u, int v) {
	return unusedTime[u] < unusedTime[v]; 
}
pairInt find_move() {
	
	pairInt result;
	int numBest = 1;
	int minCost = currentVertex.size();
	vecInt perm;
	for (auto u : uncolorSet)
		perm.push_back(u);
	sort(perm.begin(), perm.end(), timeComp);

	for (auto u : perm) {
		for (int color = 0; color < colorCount; color++) {
			if (conflict[u][color] <= minCost) {
				if (!is_tabu(u, color) || 
					(!conflict[u][color] && 
					uncolorSet.size() == bestSol.back())) {
					if (conflict[u][color] < minCost)
						numBest = 1;
					else 
						numBest++;

					if (!(rand() % (numBest * numBest))) {
						minCost = conflict[u][color];
						result = make_pair(u, color);
					}
				}
			}
		}
	}

	if (minCost == currentVertex.size()) {
		result = make_pair(rand_uncolor(), rand() % colorCount);
	}
	return result;
}

void store_solution(int sol) {
	if (bestSol.size() != currentVertex.size() + 1)
		bestSol.resize(currentVertex.size() + 1);
	
	for (int i = 0; i < currentVertex.size(); i++)
		bestSol[i] = vColor[currentVertex[i]];
	bestSol[currentVertex.size()] = sol;
}

void  recover_min_solution() {
	if (currentVertex.empty())
		return ;
	uncolorSet.clear();
	for (int i = 0; i < currentVertex.size(); i++)
		color_vertex(currentVertex[i], bestSol[i]);
}

void local_search(int iterations) {
	int steps = 0;
	int csteps = 0;
	int minSol = currentVertex.size();
	int maxSol = 0;

	while (steps < iterations) {
		pairInt move = find_move();
		if (csteps % 80000 == 0)
			move = random_find_move(minSol * colorCount);
		// 寻找一个候选解

		//步进
		int u = move.first, color = move.second;
		int objdel = conflict[u][color];
		color_vertex(u, color);
		int tTenure = randomTenure ? rand() % (tabuTenure * 2) : tabuTenure;
		for (auto v : currentAdjacency[u]) {
			tabuList[v][color] = now + tTenure;
			if (vColor[v] == color)
				color_vertex(v, -1);
		}

		if (uncolorSet.size() < bestSol.back()) {
			csteps = 0;
			store_solution(uncolorSet.size());
		}
			
		if (!uncolorSet.size())
			return ;
		// reactive tabu tenure
		minSol = min((int)uncolorSet.size(), minSol);
		maxSol = max((int)uncolorSet.size(), maxSol);

		if (csteps && csteps % frequency == 0) {
			int delta = maxSol - minSol;
			if (delta < threshold || tabuTenure == 0) {
				tabuTenure += increment;
				if (pairCycles == nextPair) {
					int p = rand() % numPairs;
					frequency = pairs[p][0];
					increment = pairs[p][1];
					nextPair = pairs[p][2];
					pairCycles = 0;
				} else
					pairCycles++;
				randomTenure = rand() & 1;
			} else if (tabuTenure > 1)
				tabuTenure--;
			
			minSol = currentVertex.size();
			maxSol = 0;

			#ifdef DEBUG
			if (csteps % 10000 == 0)
			cout << "steps = " << steps 
				<< "  " << csteps
				<< "  objdel = " << objdel
				<< "  objcheck = " << conflict[u][color]
				<< "  obj = " << uncolorSet.size()
				<< "  best = " << bestSol.back()
				<< "  delta = " << delta
				<< "  tenure = " << tabuTenure
				<< endl;
			#endif
		}

		now++; steps++; csteps++;
	}
}

inline bool hasConflict(int u) {
	for (auto v : adjacencyList[u])
		if (vColor[v] == vColor[u])
			return true;
	return false;
}

void check() {
	for (int u = 0; u < vertexCount; u++) {
		if (currentAdjacency[u].size() != adjacencyList[u].size()) {
			puts("adj error!");
			exit(-1);
		}

		for (auto v : adjacencyList[u]) {
			if (vColor[v] != -1) 
				conflict[u][vColor[v]]--;
		}

		for (int c = 0; c < colorCount; c++)
			if (conflict[u][c]) {
				puts("color error!");
				exit(-1);
			}
	}
}
void greed() {
	recover_min_solution();

	check();

	actualColCount = colorCount;
	while (!uncolorSet.empty()) {
		int u = rand_uncolor();
		for (int color = 0; color < actualColCount; color++) {
			vColor[u] = color;
			if (!hasConflict(u))
				break ;
		}

		if (hasConflict(u))
			vColor[u] = actualColCount++;
		uncolorSet.erase(u);
	}
}

int main(int argc, char *argv[]) {
	/*
	if (argc < 2) {
		puts("command format error.");
		exit(1);
	}
	*/

	int optc;
	string infile, outfile;
	FILE *outFile;
	while ((optc = getopt(argc, argv, "c:i:o:k")) != -1) {
		switch(optc) {
			case 'c':
				colorCount = atoi(optarg);
				break ;
			case 'i':
				infile = string(optarg);
				break ;
			case 'o':
				outfile = string(optarg); 
				break ;
			case 'k':
				KCORE = true;
				break ;
		}
	}
	if (outfile.length())
		outFile = fopen(outfile.c_str(), "w");
	else outFile = fopen("output", "w");

	initialize(infile);	

	kcore_decomposition();

	binPos[maxDegree + 1] = vertexCount;
	for (int k = maxDegree; k >= 0; k--) {
		if (binPos[k] == binPos[k + 1])
			continue ;
#ifdef DEBUG
		printf("Working in %d-core\n", k);
#endif 

		// 应用在上一个k-core中搜索到的最优解
		recover_min_solution();	

		// 随机地扩展候选解
		int extendCount = binPos[k + 1] - binPos[k];
		vecInt extendVertex;
		for (int i = binPos[k]; i < binPos[k + 1]; i++) {
			int u = vSorted[i];
			extendVertex.push_back(u);
		}
		random_shuffle(extendVertex.begin(), extendVertex.end());
		
		for (auto u : extendVertex) {
			inSearch[u] = true;
			currentVertex.push_back(u);
			
			for (auto v : adjacencyList[u])
				if (inSearch[v]) {
					currentAdjacency[u].push_back(v);
					currentAdjacency[v].push_back(u);
					if (vColor[v] >= 0) 
						conflict[u][vColor[v]]++;
				}
				
			for (int i = 0; i < colorCount; i++)
				if (!color_cost(u, i)) {
					color_vertex(u, i);
					break ;
				}
			if (vColor[u] == -1) uncolorSet.insert(u);
		}

		store_solution(uncolorSet.size());
		if (KCORE || currentVertex.size() == vertexCount)
			local_search((int)((currentVertex.size() == vertexCount ? 200 : 20) * currentVertex.size() * currentVertex.size()));
	}

	greed();
	fprintf(outFile, "color use: %d\n", actualColCount);
	for (int i = 0; i < vertexCount; i++)
		fprintf(outFile, "%d\n", vColor[i]);
	fclose(outFile);	

	return 0;
}