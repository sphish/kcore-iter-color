import os
import openpyxl

workbook = openpyxl.Workbook()
sheet = workbook.active

dataFolder = "dimacs_color"
dataList = os.listdir(dataFolder)

row = 0
for dataFile in dataList:
    print(dataFile)
    os.system("copy " + dataFolder + "\\" + dataFile + " input")
    os.system("a.exe")

    row += 1
    sheet.cell(row = row, column = 1, value = dataFile)
    col = 1
    vertexCount = 0
    f = open("output", "r")
    for line in f:
        argv = line.split()
        if len(argv) == 1:
            vertexCount = int(argv[0])
        else:
            col += 1
            k = int(argv[0])
            kCount = int(argv[1])
            ratio = kCount / vertexCount
            sheet.cell(row = row, column = col, value = ratio)

    f.close()

workbook.save("result.xlsx")


    

