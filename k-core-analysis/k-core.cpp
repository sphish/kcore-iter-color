#include <bits/stdc++.h>
using namespace std;

typedef vector<int> vecInt;
typedef pair<int, int> pairInt;
// graph info
const int numberDelta = -1;
const int seed = 1;
int vertexCount;
vector<vecInt> adjacencyList;
// k-core
vecInt kCore, vSorted, vDegree, binPos;
int maxDegree;
// search argv and memory
const int colorCount = 3;
const int maxSteps = 10000;
const int pickCount = 10;
const int tabuTenure = 0;
int now, conflict, actualColCount;
vecInt currentVertex, vColor, minSol;
vector<vecInt> currentAdjacency;
vector<vecInt> tabuList;
vector<bool> inSearch;

void parse_input(char *fileName) {
	ifstream inFile(fileName);
	if (!inFile.is_open()) {
		puts("inFile error");
		exit(1);
	}


	//get vertex count
	string line;
	istringstream is;
	string p, tmp;
	do {
		getline(inFile, line);
		is.clear();
		is.str(line);
		is >> p >> tmp >> vertexCount;
	} while(p != "p");

	adjacencyList.resize(vertexCount);

	int u, v;
	while (inFile >> tmp >> u >> v) {
		u += numberDelta;
		v += numberDelta;
		adjacencyList[u].push_back(v);
		adjacencyList[v].push_back(u);
	}
	inFile.close();

	// 除出重边
	for (int i = 0; i < vertexCount; i++) {
		vecInt &v = adjacencyList[i];
		sort(v.begin(), v.end());
		auto it = unique(v.begin(), v.end());
		v.resize(distance(v.begin(), it));
	}
}

void kcore_decomposition() {
	static vecInt vertexIndex(vertexCount);
	binPos.resize(vertexCount + 1);
	vDegree.resize(vertexCount);
	vSorted.resize(vertexCount);

	// sort vertexs by their degrees
	maxDegree = 0;
	for (int u = 0; u < vertexCount; u++) {
		binPos[vDegree[u] = adjacencyList[u].size()]++;
		maxDegree = max(maxDegree, vDegree[u]);
	}

	for (int s = 0, i = 0; i <= maxDegree; i++) {
		int temp = binPos[i];
		binPos[i] = s;
		s += temp;
	}

	for (int u = 0; u < vertexCount; u++) {
		int index = binPos[vDegree[u]]++;
		vertexIndex[u] = index;
		vSorted[index] = u;	
	}	

	for (int i = maxDegree; i; i--)
		binPos[i] = binPos[i - 1];
	binPos[0] = 0;

	// collapse
	for (auto u : vSorted) {
		for (auto v : adjacencyList[u]) {
			if (vDegree[v] > vDegree[u]) {
				int vIndex = vertexIndex[v];
				int wIndex = binPos[vDegree[v]];
				int w = vSorted[wIndex];
				vSorted[vIndex] = w; 
				vSorted[wIndex] = v;
				vertexIndex[v] = wIndex;
				vertexIndex[w] = vIndex;
				binPos[vDegree[v]--]++;
			}
		}
	}

//	kCore = binPos;
//	kCore.resize(maxDegree + 1);
}

void initialize(char *inFile) {
	parse_input(inFile);

	if (seed == -1)
		srand(time(NULL));
	else 
		srand(seed);

	vColor.resize(vertexCount, -1);
	inSearch.resize(vertexCount);
	currentAdjacency.resize(vertexCount);

	vecInt tmp;
	tmp.resize(colorCount, -tabuTenure);
	tabuList.resize(vertexCount, tmp);
}

inline int color_cost(int u, int color) {
	int result = 0;

	for (auto v : currentAdjacency[u]) 
		if (vColor[u] == vColor[v])
			result--;
		else if (vColor[v] == color)
			result++;
	
	return result;
}

inline void color_vertex(int u, int color) {
	conflict += color_cost(u, color);
	vColor[u] = color;
}

inline bool is_tabu(int u, int color) {
	return (now - tabuList[u][color]) < tabuTenure;
}

pairInt find_move() {
	int minCost = vertexCount;
	pairInt result;

	for (int i = 0; i < pickCount; ) {
		int u = currentVertex[rand() % currentVertex.size()];
		int newColor = rand() % colorCount;
		if (newColor == vColor[u] || is_tabu(u, newColor))
			continue ;
		
		// 计算这个候选解对目标函数的影响
		int uCost = color_cost(u, newColor);
		if (uCost < minCost) {
			uCost = minCost;
			result = make_pair(u, newColor);
		}
		i++;
	}

	return result;
}

void store_solution(int minConflict) {
	if (minSol.size() != currentVertex.size() + 1)
		minSol.resize(currentVertex.size() + 1);
	
	for (int i = 0; i < currentVertex.size(); i++)
		minSol[i] = vColor[currentVertex[i]];
	minSol[currentVertex.size()] = minConflict;
}

void  recover_min_solution() {
	if (currentVertex.empty())
		return ;
	for (int i = 0; i < currentVertex.size(); i++)
		vColor[currentVertex[i]] = minSol[i];
	conflict = minSol.back();
}

void local_search() {
	int steps = 0, minConflict = conflict;
	store_solution(conflict);

	while (conflict && steps < maxSteps) {
		now++; steps++;
		pairInt move = find_move();
		// 寻找一个候选解

		//步进
		int u = move.first, color = move.second;
		tabuList[u][vColor[u]] = now;
		color_vertex(u, color);

		if (conflict < minConflict)
			store_solution(minConflict = conflict);
	}
}

inline bool hasConflict(int u) {
	for (auto v : adjacencyList[u])
		if (vColor[v] == vColor[u])
			return true;

	return false;
}

void greed() {
	recover_min_solution();

	actualColCount = colorCount;
	for (int u = 0; u < vertexCount; u++) {
		if (!hasConflict(u)) 
			continue ;
		for (int color = 0; color < actualColCount; color++) {
			vColor[u] = color;
			if (!hasConflict(u))
				break ;
		}

		if (hasConflict(u))
			vColor[u] = actualColCount++;
	}
}

int main(int argc, char *argv[]) {
	/*
	if (argc < 2) {
		puts("command format error.");
		exit(1);
	}
	*/
	freopen("output", "w", stdout);
	char infile[20] = "input";
	initialize(infile);
	printf("%d\n", vertexCount);

	kcore_decomposition();

	binPos[maxDegree + 1] = vertexCount;
	for (int k = maxDegree; k >= 0; k--) {
		if (binPos[k] == binPos[k + 1])
			continue ;

		printf("%d %d\n", k, vertexCount - binPos[k]);
/*
		// 应用在上一个k-core中搜索到的最优解
		recover_min_solution();	

		// 随机地扩展候选解
		for (int i = binPos[k]; i < binPos[k + 1]; i++) {
			int u = vSorted[i];
			inSearch[u] = true;
			currentVertex.push_back(u);
			for (auto v : adjacencyList[u])
				if (inSearch[v]) {
					currentAdjacency[u].push_back(v);
					currentAdjacency[v].push_back(u);
				}
			color_vertex(u, rand() % colorCount);
		}

		local_search();
*/
	}

	// 使用naive的贪心生成完整解
	fclose(stdout);
	return 0;
}