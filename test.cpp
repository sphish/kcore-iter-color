#include <bits/stdc++.h>
using namespace std;

int main() {
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	auto l = v.begin() + 2;
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	random_shuffle(l, v.end());
	for (auto e = l; e != v.end(); e++)
		cout << *e << endl;
	return 0;
}