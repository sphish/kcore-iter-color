import os
import openpyxl
import time

workbook = openpyxl.Workbook()
sheet = workbook.active

dataFolder = "dimacs_color"
dataList = os.listdir(dataFolder)
ccDict = {"DSJC1000.1.col" : [21],
    "DSJC1000.5.col" : [91],
    "DSJC1000.9.col" : [230],
    "DSJC500.1.col" : [12],
    "DSJC500.5.col" : [48, 50],
    "DSJC500.9.col" : [126, 128],
    "DSJR500.1c.col" : [85],
    "DSJC500.5.col" : [122, 128],
    "flat1000_50_0.col" : [50],
    "flat1000_60_0.col" : [60],
    "flat1000_76_0.col" : [90],
    "flat300_28_0.col" : [28, 31],
    "le450_15c.col" : [15, 16],
    "le450_15d.col" : [15, 16],
    "le450_25c.col" : [25, 27],
    "le450_25d.col" : [25, 27]}

row = 0

resultFile = open("result.txt", "w")
for dataFile in dataList:
    if (dataFile not in  ccDict.keys()):
        continue

    print(dataFile)
    for cc in ccDict[dataFile]:
        row += 1
        sheet.cell(row = row, column = 1, value = dataFile)

        minNumber = 0
        minCount = 1000000000
        averageTime = 0.0

        for i in range(10):
            startTime = time.time()
            try:
                os.system("a.exe -i " + dataFolder + "/" + dataFile + " -k -c " + str(cc))
            except e:
                pass
            runTime = time.time() - startTime 
            f = open("output", "r")
            line = f.readline()
            try:
                ans = int((line.split())[2])
            except:
                f.close()
                continue 
            if ans == minCount:
                minNumber += 1
                averageTime += runTime
            elif ans < minCount:
                minNumber = 1
                minCount = ans
                averageTime = runTime
            print(line, ans, runTime)
            f.close()

        print(dataFile + " min ans: " + str(minCount) + " success times: " + str(minNumber) + " average time: " + str(averageTime / minNumber))

        sheet.cell(row = row, column = 2, value = minCount)
        sheet.cell(row = row, column = 3, value = minNumber)
        sheet.cell(row = row, column = 4, value = averageTime / minNumber)

workbook.save("result.xlsx")
resultFile.close()

    

